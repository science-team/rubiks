#include <stdio.h>

#define EQUIV
#define NOMAX
#include "trans/inv.a"
#include "trans/perm8.a"
#ifdef CTWIST
#include "trans/cperm.a"
#include "trans/cperm.r"
#include "trans/cperm.m"
#include "trans/twist.a"
#include "trans/twist.r"
#include "trans/twist.c"
#endif /* CTWIST */
#ifdef CCPERM
#include "trans/twist.a"
#include "trans/twist.r"
#include "trans/twist.m"
#include "trans/cperm.a"
#include "trans/cperm.r"
#include "trans/cperm.c"
#endif /* CCPERM */

#ifdef CTWIST
#define MAX_TURNS    (MAX_CPERMS * TWIST_MAGIC)
#endif /* CTWIST */
#ifdef CCPERM
#define MAX_TURNS    (MAX_TWISTS * CPERM_MAGIC)
#endif /* CCPERM */

#include "longtype.h"

TYPE turns1[MT];
TYPE turns2[MT];
long left = MAX_TURNS;
int quarter;
int maximal;
int gcount;
int givemax;

do_pr(turns, count)
int turns, count;
{
    if(maximal != 0 && turns != 1) {
	printf(", %10d maximal", maximal);
    }
    printf("\n");
    gcount = count;
    printf("%10d with %2d turns", count, turns);
    fflush(stdout);
}

try_one(num, count)
int num, count;
{
#ifdef CTWIST
int twist, ntwist, ntwist1, twist0;
int cperm, ncperm, ncperm1;
#endif /* CTWIST */
#ifdef CCPERM
int cperm, ncperm, ncperm1, cperm0;
int twist, ntwist, ntwist1;
#endif /* CCPERM */
int i, j, nnum, counted = 0, k, l, max, wcounted = 0, m;

#ifdef CTWIST
    twist0 = num / MAX_CPERMS;
    cperm = num - twist0 * MAX_CPERMS;
    max = twists_weights[twist0];
    twist = twists_presents[twist0];
#endif /* CTWIST */
#ifdef CCPERM
    cperm0 = num / MAX_TWISTS;
    twist = num - cperm0 * MAX_TWISTS;
    max = cperms_weights[cperm0];
    cperm = cperms_presents[cperm0];
#endif /* CCPERM */
    for(i = 0; i < 6; i++) {
	ntwist = twist;
	ncperm = cperm;
	for(j = 0; j < 3; j++) {
	    ntwist = twists[ntwist][i];
	    ncperm = cperms[ncperm][i];
	    if(quarter && j == 1) {
		continue;
	    }
#ifdef CTWIST
	    ntwist1 = twists_pointer[ntwist];
	    for(m = 0; m < MI; m++) {
		if(twists_rotate[ntwist][m]) {
		    ncperm1 = cperms_trans[ncperm][m];
		    nnum = ntwist1 * MAX_CPERMS + ncperm1;
#endif /* CTWIST */
#ifdef CCPERM
	    ncperm1 = cperms_pointer[ncperm];
	    for(m = 0; m < MI; m++) {
		if(cperms_rotate[ncperm][m]) {
		    ntwist1 = twists_trans[ntwist][m];
		    nnum = ncperm1 * MAX_TWISTS + ntwist1;
#endif /* CCPERM */
		    k = (nnum >> POW);
		    l = (1 << (nnum & MASK));
		    if((turns1[k] & l) == 0) {
			if((turns2[k] & l) == 0) {
			    turns2[k] |= l;
			    counted++;
#ifdef CTWIST
			    wcounted += twists_weights[ntwist1];
#endif /* CTWIST */
#ifdef CCPERM
			    wcounted += cperms_weights[ncperm1];
#endif /* CCPERM */
			}
			max = 0;
		    }
		}
	    }
	}
    }
    left -= counted;
    maximal += max;
    return wcounted;
}

try(count)
int count;
{
int i, counted = 0, k;
TYPE j;

    maximal = 0;
    for(i = 0; i < MT; i++) {
	if(turns2[i] != 0) {
	    turns1[i] |= turns2[i];
	}
    }
    for(i = 0; i < MT; i++) {
	if((turns1[i] & turns2[i]) != 0) {
	    j = turns1[i] & turns2[i];
	    turns2[i] &= (j ^ (~(TYPE)0));
	    k = 0;
	    while(j != 0) {
		if(j & 1) {
		    counted += try_one((i << POW) + k, count + 1);
		}
		k++;
		j >>= 1;
	    }
	}
    }
    do_pr(count + 1, counted);
}

main(argc, argv)
int argc;
char *argv[];
{
int i, k, error = 0;
TYPE j;

    quarter = -1;
    givemax = -1;
    if(argc > 3) {
	fprintf(stderr, "Too many parameters\n");
	exit(1);
    }
    while(argc > 1) {
	if(!strcmp(argv[1], "-q")) {
	    if(quarter >= 0) {
		error = 1;
	    }
	    quarter = 1;
	} else if(!strcmp(argv[1], "-m")) {
	    if(givemax >= 0) {
		error = 1;
	    }
	    givemax = 1;
	}
	argc--;
	argv++;
    }
    if(error) {
	fprintf(stderr, "usage: size333c [-q] [-m]\n");
	exit(1);
    }
    if(quarter < 0) {
	quarter = 0;
    }
    if(givemax < 0) {
	givemax = 0;
    }
#ifdef CTWIST
#ifdef VERBOSE
    fprintf(stderr, "Initializing permutations...\n");
#endif /* VERBOSE */
    init_twists();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Collapsing permutations...\n");
#endif /* VERBOSE */
    init_twists1();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing cperms...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching cperms...\n");
#endif /* VERBOSE */
    init_cperms2();
#endif /* CTWIST */
#ifdef CCPERM
#ifdef VERBOSE
    fprintf(stderr, "Initializing permutations...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Collapsing permutations...\n");
#endif /* VERBOSE */
    init_cperms1();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing twists...\n");
#endif /* VERBOSE */
    init_twists();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Matching twists...\n");
#endif /* VERBOSE */
    init_twists2();
#endif /* CCPERM */
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing data array...\n");
#endif /* VERBOSE */
    for(i = 0; i < MT; i++) {
	turns1[i] = turns2[i] = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
#endif /* VERBOSE */
    turns2[0] = 1;
    left--;
    do_pr(0, 1);
    i = 0;
    while(left > 0) {
	try(i++);
    }
    printf(", %10d maximal\n", gcount);
    if(givemax) {
	printf("Maximal configurations:\n");
	for(i = 0; i < MT; i++) {
	    if(turns2[i] != 0) {
		turns1[i] |= turns2[i];
	    }
	}
	for(i = 0; i < MT; i++) {
	    if((turns1[i] & turns2[i]) != 0) {
		j = turns1[i] & turns2[i];
		turns2[i] &= (j ^ (~(TYPE)0));
		k = 0;
		while(j != 0) {
		    if(j & 1) {
			printf("%8d\n", (i << POW) + k);
		    }
		    k++;
		    j >>= 1;
		}
	    }
	}
    }
    exit(0);
}

