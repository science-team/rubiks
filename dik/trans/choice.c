#ifndef ROT2
#define CHOICE_MAGIC	81
#else /* ROT2 */
#define CHOICE_MAGIC	45
#endif /* ROT2 */

static unsigned short choices_pointer[MAX_CHOICES];
static char choices_weights[CHOICE_MAGIC];
static unsigned short choices_presents[CHOICE_MAGIC];
static int choices_magic_counter;
#ifdef EQUIV
static char choices_rotate[MAX_CHOICES][MI];
#endif /* EQUIV */

static void init_choices1()
{
int i, j, k;
int c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, ca, cb;
int counted, val;
#define params	c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb
#define params1	&c0,&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&ca,&cb

    counted = 0;
    for(i = 0; i < MAX_CHOICES; i++) {
	choices_pointer[i] = 65535;
    }
    for(i = 0; i < MAX_CHOICES; i++) {
	if(choices_pointer[i] != 65535) {
	    choices_weights[choices_pointer[i]]++;
	    continue;
	}
	choices_presents[counted] = i;
	choices_pointer[i] = counted;
	choices_weights[counted] = 1;
#ifdef EQUIV
	choices_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	val_choice(i, params1);
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	flip_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_choices(params1);
	j = choice_val(params);
	if(choices_pointer[j] == 65535) {
	    choices_pointer[j] = counted;
	}
#ifdef EQUIV
	choices_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	counted++;
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    choices_magic_counter = counted;
}
#undef inc
#undef params
#undef params1

