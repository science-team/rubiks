static unsigned short choices_trans[MAX_CHOICES][MI];

static void init_choices2()
{
int i, k;
int c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb;
#define params	c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb
#define params1	&c0,&c1,&c2,&c3,&c4,&c5,&c6,&c7,&c8,&c9,&ca,&cb

    for(i = 0; i < MAX_CHOICES; i++) {
	val_choice(i,params1);
	choices_trans[i][0] = i;
	k = 0;
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#ifndef ROT2
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#ifndef ROT2
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	flip_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#ifndef ROT2
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#endif /* ROT2 */
	rotate_ud_choices(params1);
	rotate_rl_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#ifndef ROT2
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
	rotate_ud_choices(params1);
	choices_trans[i][inv_trans[++k]] = choice_val(params);
#endif /* ROT2 */
    }
}
#undef params
#undef params1

