#ifndef IPERM
#define EPERM_MAGIC	2768
#else /* IPERM */
#define EPERM_MAGIC	1672
#endif /* IPERM */

static unsigned short eperms_pointer[MAX_EPERMS];
static char eperms_weights[EPERM_MAGIC];
static unsigned short eperms_presents[EPERM_MAGIC];
static int eperms_magic_counter;
#ifdef EQUIV
static char eperms_rotate[MAX_EPERMS][MI];
#endif /* EQUIV */

static void init_eperms1()
{
int i, j, k;
int p[8];
int counted;
#define params	p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]
#define params1	p+0,p+1,p+2,p+3,p+4,p+5,p+6,p+7

    counted = 0;
    for(i = 0; i < MAX_EPERMS; i++) {
	eperms_pointer[i] = 65535;
    }
    for(i = 0; i < MAX_EPERMS; i++) {
	if(eperms_pointer[i] != 65535) {
	    eperms_weights[eperms_pointer[i]]++;
	    continue;
	}
	val_perm8(i,params1);
	eperms_presents[counted] = i;
	eperms_pointer[i] = counted;
	eperms_weights[counted] = 1;
#ifdef EQUIV
	eperms_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifdef IPERM
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	inv_perm8(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_eperms(p);
	j = perm8_val(params);
	if(eperms_pointer[j] == 65535) {
	    eperms_pointer[j] = counted;
	}
#ifdef EQUIV
	eperms_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* IPERM */
	counted++;
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    eperms_magic_counter = counted;
}
#undef params
#undef params1

