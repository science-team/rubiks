#define inv(i) ((3-(i)) % 3)

void rotate_ud_twists(t0,t1,t2,t3,t4,t5,t6,t7)
int *t0,*t1,*t2,*t3,*t4,*t5,*t6,*t7;
{
int tmp;

#ifndef ROT2
    tmp = *t0;
    *t0 = *t3;
    *t3 = *t2;
    *t2 = *t1;
    *t1 = tmp;
    tmp = *t4;
    *t4 = *t7;
    *t7 = *t6;
    *t6 = *t5;
    *t5 = tmp;
#else /* ROT2 */
    tmp = *t0;
    *t0 = *t2;
    *t2 = tmp;
    tmp = *t1;
    *t1 = *t3;
    *t3 = tmp;
    tmp = *t4;
    *t4 = *t6;
    *t6 = tmp;
    tmp = *t5;
    *t5 = *t7;
    *t7 = tmp;
#endif /* ROT2 */
}

void rotate_rl_twists(t0,t1,t2,t3,t4,t5,t6,t7)
int *t0,*t1,*t2,*t3,*t4,*t5,*t6,*t7;
{
int tmp;

    tmp = *t0;
    *t0 = *t7;
    *t7 = tmp;
    tmp = *t1;
    *t1 = *t6;
    *t6 = tmp;
    tmp = *t2;
    *t2 = *t5;
    *t5 = tmp;
    tmp = *t3;
    *t3 = *t4;
    *t4 = tmp;
}

void flip_twists(t0,t1,t2,t3,t4,t5,t6,t7)
int *t0,*t1,*t2,*t3,*t4,*t5,*t6,*t7;
{
int tmp;

    tmp = inv(*t0);
    *t0 = inv(*t4);
    *t4 = tmp;
    tmp = inv(*t1);
    *t1 = inv(*t5);
    *t5 = tmp;
    tmp = inv(*t2);
    *t2 = inv(*t6);
    *t6 = tmp;
    tmp = inv(*t3);
    *t3 = inv(*t7);
    *t7 = tmp;
}
#undef inv

