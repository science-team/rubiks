#ifndef IPERM
#define CPERM_MAGIC	2768
#else /* IPERM */
#define CPERM_MAGIC	1672
#endif /* IPERM */

static unsigned short cperms_pointer[MAX_CPERMS];
static char cperms_weights[CPERM_MAGIC];
static unsigned short cperms_presents[CPERM_MAGIC];
static int cperms_magic_counter;
#ifdef EQUIV
static char cperms_rotate[MAX_CPERMS][MI];
#endif /* EQUIV */

static void init_cperms1()
{
int i, j, k;
int p[8];
int counted;
#define params	p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]
#define params1	p+0,p+1,p+2,p+3,p+4,p+5,p+6,p+7

    counted = 0;
    for(i = 0; i < MAX_CPERMS; i++) {
	cperms_pointer[i] = 65535;
    }
    for(i = 0; i < MAX_CPERMS; i++) {
	if(cperms_pointer[i] != 65535) {
	    cperms_weights[cperms_pointer[i]]++;
	    continue;
	}
	val_perm8(i,params1);
	cperms_presents[counted] = i;
	cperms_pointer[i] = counted;
	cperms_weights[counted] = 1;
#ifdef EQUIV
	cperms_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifdef IPERM
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	inv_perm8(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	flip_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	rotate_rl_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_cperms(p);
	j = perm8_val(params);
	if(cperms_pointer[j] == 65535) {
	    cperms_pointer[j] = counted;
	}
#ifdef EQUIV
	cperms_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* IPERM */
	counted++;
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    cperms_magic_counter = counted;
}
#undef params
#undef params1

