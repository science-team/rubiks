#ifndef ROT2
#define TWIST_MAGIC	168
#else /* ROT2 */
#define TWIST_MAGIC	324
#endif /* ROT2 */

static unsigned short twists_pointer[MAX_TWISTS];
static char twists_weights[TWIST_MAGIC];
static unsigned short twists_presents[TWIST_MAGIC];
static int twists_magic_counter;
#ifdef EQUIV
static char twists_rotate[MAX_TWISTS][MI];
#endif /* EQUIV */

static void init_twists1()
{
int i, j, k;
int t0, t1, t2, t3, t4, t5, t6, t7;
int nt0, nt1, nt2, nt3, nt4, nt5, nt6, nt7;
int counted, val;
#define inc(i, j) ((i+j) % 3)
#define params	nt0,nt1,nt2,nt3,nt4,nt5,nt6,nt7
#define params1	&nt0,&nt1,&nt2,&nt3,&nt4,&nt5,&nt6,&nt7

    counted = 0;
    for(i = 0; i < MAX_TWISTS; i++) {
	twists_pointer[i] = 65535;
    }
    t0 = t1 = t2 = t3 = t4 = t5 = t6 = t7 = 0;
    for(i = 0; i < MAX_TWISTS; i++) {
	if(twists_pointer[i] != 65535) {
	    twists_weights[twists_pointer[i]]++;
	    goto fin;
	}
	twists_presents[counted] = i;
	twists_pointer[i] = counted;
	twists_weights[counted] = 1;
#ifdef EQUIV
	twists_rotate[i][0] = 1;
	k = 0;
#endif /* EQUIV */
	nt0 = t0;
	nt1 = t1;
	nt2 = t2;
	nt3 = t3;
	nt4 = t4;
	nt5 = t5;
	nt6 = t6;
	nt7 = t7;
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	flip_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	rotate_ud_twists(params1);
	rotate_rl_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#ifndef ROT2
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
	rotate_ud_twists(params1);
	j = twist_val(params);
	if(twists_pointer[j] == 65535) {
	    twists_pointer[j] = counted;
	}
#ifdef EQUIV
	twists_rotate[j][++k] = 1;
#endif /* EQUIV */
#endif /* ROT2 */
	counted++;
fin:
	t0 = inc(t0,1);
	if(t0 == 0) {
	    t1 = inc(t1,1);
	    if(t1 == 0) {
		t2 = inc(t2,1);
		if(t2 == 0) {
		    t3 = inc(t3,1);
		    if(t3 == 0) {
			t4 = inc(t4,1);
			if(t4 == 0) {
			    t5 = inc(t5,1);
			    if(t5 == 0) {
				t6 = inc(t6,1);
			    }
			}
		    }
		}
	    }
	}
	j = (t0 + t1 + t2 + t3 + t4 + t5 + t6) % 3;
	t7 = 3 - j;
	if(t7 == 3) {
	    t7 = 0;
	}
    }
#ifdef VERBOSE
    fprintf(stderr, "\tLeft = %d\n", counted);
#endif /* VERBOSE */
    twists_magic_counter = counted;
}
#undef inc
#undef params
#undef params1

