static unsigned short sperms_trans[MAX_SPERMS][MI];

static void init_sperms2()
{
int i, k;
int p[4];
#define params	p[0],p[1],p[2],p[3]
#define params1	p+0,p+1,p+2,p+3

    for(i = 0; i < MAX_SPERMS; i++) {
	val_perm4(i,params1);
	sperms_trans[i][0] = i;
	k = 0;
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	flip_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
#ifdef IPERM
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	flip_sperms(p);
	inv_perm4(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	flip_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	rotate_rl_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
	rotate_ud_sperms(p);
	sperms_trans[i][inv_trans[++k]] = perm4_val(params);
#endif /* IPERM */
    }
}
#undef params
#undef params1

