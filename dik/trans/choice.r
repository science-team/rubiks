void rotate_ud_choices(c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb)
int *c0,*c1,*c2,*c3,*c4,*c5,*c6,*c7,*c8,*c9,*ca,*cb;
{
int tmp;

#ifndef ROT2
    tmp = *c0;
    *c0 = *c3;
    *c3 = *c2;
    *c2 = *c1;
    *c1 = tmp;
    tmp = *c4;
    *c4 = *c7;
    *c7 = *c6;
    *c6 = *c5;
    *c5 = tmp;
    tmp = *c8;
    *c8 = *cb;
    *cb = *ca;
    *ca = *c9;
    *c9 = tmp;
#else /* ROT2 */
    tmp = *c0;
    *c0 = *c2;
    *c2 = tmp;
    tmp = *c1;
    *c1 = *c3;
    *c3 = tmp;
    tmp = *c4;
    *c4 = *c6;
    *c6 = tmp;
    tmp = *c5;
    *c5 = *c7;
    *c7 = tmp;
    tmp = *c8;
    *c8 = *ca;
    *ca = tmp;
    tmp = *c9;
    *c9 = *cb;
    *cb = tmp;
#endif /* ROT2 */
}

void rotate_rl_choices(c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb)
int *c0,*c1,*c2,*c3,*c4,*c5,*c6,*c7,*c8,*c9,*ca,*cb;
{
int tmp;

    tmp = *c0;
    *c0 = *c6;
    *c6 = tmp;
    tmp = *c1;
    *c1 = *c5;
    *c5 = tmp;
    tmp = *c2;
    *c2 = *c4;
    *c4 = tmp;
    tmp = *c3;
    *c3 = *c7;
    *c7 = tmp;
    tmp = *c8;
    *c8 = *cb;
    *cb = tmp;
    tmp = *c9;
    *c9 = *ca;
    *ca = tmp;
}

void flip_choices(c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ca,cb)
int *c0,*c1,*c2,*c3,*c4,*c5,*c6,*c7,*c8,*c9,*ca,*cb;
{
int tmp;

    tmp = *c0;
    *c0 = *c4;
    *c4 = tmp;
    tmp = *c1;
    *c1 = *c5;
    *c5 = tmp;
    tmp = *c2;
    *c2 = *c6;
    *c6 = tmp;
    tmp = *c3;
    *c3 = *c7;
    *c7 = tmp;
}

