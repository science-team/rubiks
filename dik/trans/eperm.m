static unsigned short eperms_trans[MAX_EPERMS][MI];

static void init_eperms2()
{
int i, k; int l;
int p[8];
#define params	p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]
#define params1	p+0,p+1,p+2,p+3,p+4,p+5,p+6,p+7

    for(i = 0; i < MAX_EPERMS; i++) {
	val_perm8(i,params1);
	eperms_trans[i][0] = i;
	k = 0;
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
#ifdef IPERM
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	inv_perm8(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	flip_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	rotate_rl_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
	rotate_ud_eperms(p);
	eperms_trans[i][inv_trans[++k]] = perm8_val(params);
#endif /* IPERM */
    }
}
#undef params
#undef params1

