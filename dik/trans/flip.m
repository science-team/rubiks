static unsigned short flips_trans[MAX_FLIPS][MI];

#ifndef ROT2
ERROR: Collapsing with FLIP used requires ROT2
#endif /* ROT2 */

static void init_flips2()
{
int i, j, k;
int f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb;
int nf0,nf1,nf2,nf3,nf4,nf5,nf6,nf7,nf8,nf9,nfa,nfb;
#define params	nf0,nf1,nf2,nf3,nf4,nf5,nf6,nf7,nf8,nf9,nfa,nfb
#define params1	&nf0,&nf1,&nf2,&nf3,&nf4,&nf5,&nf6,&nf7,&nf8,&nf9,&nfa,&nfb
#define inv(i)	((i)^1)

    f0 = f1 = f2 = f3 = f4 = f5 = f6 = f7 = f8 = f9 = fa = fb = 0;
    for(i = 0; i < MAX_FLIPS; i++) {
	nf0 = f0;
	nf1 = f1;
	nf2 = f2;
	nf3 = f3;
	nf4 = f4;
	nf5 = f5;
	nf6 = f6;
	nf7 = f7;
	nf8 = f8;
	nf9 = f9;
	nfa = fa;
	nfb = fb;
	flips_trans[i][0] = i;
	k = 0;
	rotate_ud_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	flip_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	rotate_rl_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	rotate_ud_flips(params1);
	flips_trans[i][inv_trans[++k]] = flip_val(params);
	f0 = inv(f0);
	if(f0 == 0) {
	    f1 = inv(f1);
	    if(f1 == 0) {
		f2 = inv(f2);
		if(f2 == 0) {
		    f3 = inv(f3);
		    if(f3 == 0) {
			f4 = inv(f4);
			if(f4 == 0) {
			    f5 = inv(f5);
			    if(f5 == 0) {
				f6 = inv(f6);
				if(f6 == 0) {
				    f7 = inv(f7);
				    if(f7 == 0) {
					f8 = inv(f8);
					if(f8 == 0) {
					    f9 = inv(f9);
					    if(f9 == 0) {
						fa = inv(fa);
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	j = (f0 + f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8 + f9 + fa) & 1;
	fb = j;
    }
}
#undef params
#undef params1
#undef inv

