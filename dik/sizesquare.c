#include <stdio.h>

#define NOMAX
#include "trans/perm4.a"
#include "trans/eperm2.a"
#include "trans/cperm2.a"

#define MAX_TURNS    (MAX_EPERMS2 * MAX_CPERMS2)

#include "longtype.h"

TYPE turns1[MT];
TYPE turns2[MT];
long left = MAX_TURNS / 12;
int maximal;
int gcount;
int givemax;

do_pr(turns, count)
int turns, count;
{
    if(maximal != 0 && turns != 1) {
	printf(", %10d maximal", maximal);
    }
    printf("\n");
    gcount = count;
    printf("%10d with %2d turns", count, turns);
    fflush(stdout);
}

try_one(num, count)
int num, count;
{
int eperm, neperm;
int cperm, ncperm;
int i, j, nnum, counted = 0, k, l, max = 1;

    eperm = num / MAX_CPERMS2;
    cperm = num - eperm * MAX_CPERMS2;
    for(i = 0; i < 6; i++) {
	neperm = eperms2[eperm][i];
	ncperm = cperms2[cperm][i];
	nnum = neperm * MAX_CPERMS2 + ncperm;
	k = (nnum >> POW);
	l = (1 << (nnum & MASK));
	if((turns1[k] & l) == 0) {
	    if((turns2[k] & l) == 0) {
		turns2[k] |= l;
		counted++;
	    }
	    max = 0;
	}
    }
    left -= counted;
    maximal += max;
    return counted;
}

try(count)
int count;
{
int i, counted = 0, k;
TYPE j;

    maximal = 0;
    for(i = 0; i < MT; i++) {
	if(turns2[i] != 0) {
	    turns1[i] |= turns2[i];
	}
    }
    for(i = 0; i < MT; i++) {
	if((turns1[i] & turns2[i]) != 0) {
	    j = turns1[i] & turns2[i];
	    turns2[i] &= (j ^ (~(TYPE)0));
	    k = 0;
	    while(j != 0) {
		if(j & 1) {
		    counted += try_one((i << POW) + k, count + 1);
		}
		k++;
		j >>= 1;
	    }
	}
    }
    do_pr(count + 1, counted);
}

main(argc, argv)
int argc;
char *argv[];
{
int i, k, error = 0;
TYPE j;

    givemax = 0;
    if(argc > 2) {
	fprintf(stderr, "Too many parameters\n");
	exit(1);
    }
    while(argc > 1) {
	if(!strcmp(argv[1], "-m")) {
	    if(givemax >= 0) {
		error = 1;
	    }
	    givemax = 1;
	}
	argc--;
	argv++;
    }
    if(error) {
	fprintf(stderr, "usage: sizesquare [-m]\n");
	exit(1);
    }
#ifdef VERBOSE
    fprintf(stderr, "Initializing edge permutations...\n");
#endif /* VERBOSE */
    init_eperms2();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing corner permutations...\n");
#endif /* VERBOSE */
    init_cperms2();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing data array...\n");
#endif /* VERBOSE */
    for(i = 0; i < MT; i++) {
	turns1[i] = turns2[i] = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
#endif /* VERBOSE */
    turns2[0] = 1;
    left--;
    do_pr(0, 1);
    i = 0;
    while(left > 0) {
	try(i++);
    }
    printf(", %10d maximal\n", gcount);
    if(givemax) {
	printf("Maximal configurations:\n");
	for(i = 0; i < MT; i++) {
	    if(turns2[i] != 0) {
		turns1[i] |= turns2[i];
	    }
	}
	for(i = 0; i < MT; i++) {
	    if((turns1[i] & turns2[i]) != 0) {
		j = turns1[i] & turns2[i];
		turns2[i] &= (j ^ (~(TYPE)0));
		k = 0;
		while(j != 0) {
		    if(j & 1) {
			printf("%8d\n", (i << POW) + k);
		    }
		    k++;
		    j >>= 1;
		}
	    }
	}
    }
    exit(0);
}

