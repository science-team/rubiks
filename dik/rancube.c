#include <sys/types.h>
#include <sys/time.h>

main()
{
int i, j, k;

    srandom((long)time((time_t *)NULL));
    while(1) {
	for(i = 0; i < 100; i++) {
		k = random();
		j = k % 6;
		k = (k / 6) % 3;
		if(j < 0) j += 6;
		if(k < 0) k += 3;
		printf(" %c%d", "UDFLRB"[j], k + 1);
	}
	printf("\n");
    }
}
