#include <stdio.h>

#define NOMAX
#include "trans/perm7.a"
#include "trans/twist7.a"
#include "trans/cperm7.a"

#define MAX_TURNS    (MAX_TWISTS * MAX_CPERMS)

#include "longtype.h"

TYPE turns1[MT];
TYPE turns2[MT];
long left = MAX_TURNS;
int quarter;
int maximal;
int gcount;
int givemax;

do_pr(turns, count)
int turns, count;
{
    if(maximal != 0 && turns != 1) {
	printf(", %10d maximal", maximal);
    }
    printf("\n");
    gcount = count;
    printf("%10d with %2d turns", count, turns);
    fflush(stdout);
}

try_one(num, count)
int num, count;
{
int perm, nperm;
int twist, ntwist;
int i, j, nnum, counted = 0, k, l, max = 1;

    perm = num / MAX_TWISTS;
    twist = num - perm * MAX_TWISTS;
    for(i = 0; i < 3; i++) {
	nperm = perm;
	ntwist = twist;
	for(j = 0; j < 3; j++) {
	    nperm = cperms[nperm][i];
	    ntwist = twists[ntwist][i];
	    nnum = nperm * MAX_TWISTS + ntwist;
	    if(quarter && j == 1) {
		continue;
	    }
	    k = (nnum >> POW);
	    l = (1 << (nnum & MASK));
	    if((turns1[k] & l) == 0) {
		if((turns2[k] & l) == 0) {
		    turns2[k] |= l;
		    counted++;
		}
		max = 0;
	    }
	}
    }
    left -= counted;
    maximal += max;
    return counted;
}

try(count)
int count;
{
int i, counted = 0, k;
TYPE j;

    maximal = 0;
    for(i = 0; i < MT; i++) {
	if(turns2[i] != 0) {
	    turns1[i] |= turns2[i];
	}
    }
    for(i = 0; i < MT; i++) {
	if((turns1[i] & turns2[i]) != 0) {
	    j = turns1[i] & turns2[i];
	    turns2[i] &= (j ^ (~(TYPE)0));
	    k = 0;
	    while(j != 0) {
		if(j & 1) {
		    counted += try_one((i << POW) + k, count + 1);
		}
		k++;
		j >>= 1;
	    }
	}
    }
    do_pr(count + 1, counted);
}

main(argc, argv)
int argc;
char *argv[];
{
int i, k, error = 0;
TYPE j;

    quarter = -1;
    givemax = -1;
    if(argc > 3) {
	fprintf(stderr, "Too many parameters\n");
	exit(1);
    }
    while(argc > 1) {
	if(!strcmp(argv[1], "-q")) {
	    if(quarter >= 0) {
		error = 1;
	    }
	    quarter = 1;
	} else if(!strcmp(argv[1], "-m")) {
	    if(givemax >= 0) {
		error = 1;
	    }
	    givemax = 1;
	}
	argc--;
	argv++;
    }
    if(error) {
	fprintf(stderr, "usage: size222 [-q] [-m]\n");
	exit(1);
    }
    if(quarter < 0) {
	quarter = 0;
    }
    if(givemax < 0) {
	givemax = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Initializing twists...\n");
#endif /* VERBOSE */
    init_twists();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing permutations...\n");
#endif /* VERBOSE */
    init_cperms();
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
    fprintf(stderr, "Initializing data array...\n");
#endif /* VERBOSE */
    for(i = 0; i < MT; i++) {
	turns1[i] = turns2[i] = 0;
    }
#ifdef VERBOSE
    fprintf(stderr, "Done!\n");
#endif /* VERBOSE */
    turns2[0] = 1;
    left--;
    do_pr(0, 1);
    i = 0;
    while(left > 0) {
	try(i++);
    }
    printf(", %10d maximal\n", gcount);
    if(givemax) {
        printf("Maximal configurations:\n");
	for(i = 0; i < MT; i++) {
	    if(turns2[i] != 0) {
		turns1[i] |= turns2[i];
	    }
	}
	for(i = 0; i < MT; i++) {
	    if((turns1[i] & turns2[i]) != 0) {
		j = turns1[i] & turns2[i];
		turns2[i] &= (j ^ (~(TYPE)0));
		k = 0;
		while(j != 0) {
		    if(j & 1) {
			printf("%8d\n", (i << POW) + k);
		    }
		    k++;
		    j >>= 1;
		}
	    }
	}
    }
    exit(0);
}

